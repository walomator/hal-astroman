extends Control

var map_loader
var current_map
var players

var star_particles_node
var tile_cursor_node

# Map metadata
var starting_position = Vector2(0, 0)

# UI config
var edit_mode_enabled = false

# UI elements
var current_mouse_pos

# External scripts
const MapLoader = preload("res://scripts/platformer-framework/map_loader.gd/")
const ParticlesExtended = preload("res://scripts/platformer-framework/particles_extended.gd")

func _ready():
	star_particles_node = get_node("/root/World/StarParticles")
	tile_cursor_node = get_node("/root/World/TileCursor")
	
	map_loader = MapLoader.new(self)
	map_loader.switch_to("Map0.tscn")
	starting_position = Vector2(38, 17) # BUG - Doesn't work, and # DEV - Should be specified by maps
	

func _process(delta):
	if edit_mode_enabled:
		var new_mouse_pos = viewport_to_world_coords(get_viewport().get_mouse_position()) # Mouse position is in respect to world position
		if current_mouse_pos != new_mouse_pos:
			current_mouse_pos = new_mouse_pos
			draw_tile_cursor()
	

func _input(event):
	if event is InputEventMouseButton and event.is_pressed():
		if edit_mode_enabled:
			place_tile(viewport_to_world_coords(event.position))
	
	if event is InputEventMouseMotion:
		if edit_mode_enabled:
			draw_tile_cursor()
	

func change_map(map_name):
	map_loader.switch_to(map_name)
	

func save_current_map():
	map_loader.save_map(current_map)
	

func add_player(new_player):
	players.append(new_player)
	new_player.reset_position()
	

func toggle_edit_mode():
	edit_mode_enabled = !edit_mode_enabled
	tile_cursor_node.visible = edit_mode_enabled
	

func place_tile(mouse_pos):
	draw_tile_cursor(mouse_pos)
	
	var tile_map = current_map.get_node("TileMap")
	mouse_pos = tile_map.world_to_map(mouse_pos)
	tile_map.set_cellv(mouse_pos, 1)
	

func draw_tile_cursor(mouse_pos = current_mouse_pos):
	tile_cursor_node.global_position.x = floor(mouse_pos.x/32) * 32 # Draw the top left of the edit box at this position, aligning with the tileset
	tile_cursor_node.global_position.y = floor(mouse_pos.y/32) * 32
	

func viewport_to_world_coords(viewport_pos):
	viewport_pos -= get_viewport().canvas_transform.origin
	viewport_pos = Vector2(round(viewport_pos.x), round(viewport_pos.y))
	return viewport_pos
	
