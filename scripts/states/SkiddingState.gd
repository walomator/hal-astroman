extends "res://scripts/platformer-framework/State.gd"

# This state will not only reduce movement speed until stopped, but
# also reduce horizontal natural velocity at a rate according to
# GROUND_DRAG

# Inertia represents the direction of skidding. Reducing the player's
# natural_velocity is dependent on the direction of the remaining
# natural_velocity. The natural_velocity is the actual vector to move
# the player, so using increasing natural_velocity.x slows a player if
# it is moving left (decreasing position.x)
var inertia_direction # 1 or -1, same as player.facing_direction

func _init(controlled_player):
	player = controlled_player
	

func start():
	set_state_name("SkiddingState")
	player.idle_sprite.visible = true
	
	if (player.get_relative_velocity().x == 0) && (player.run_speed == 0):
		set_state("StandingState")
		return
	elif player.get_relative_velocity().x > 0:
		inertia_direction = 1
	elif player.get_relative_velocity().x < 0:
		inertia_direction = -1
	else:
		inertia_direction = player.facing_direction
		
	# Convert the force of movement to natural velocity
	if inertia_direction == 1:
		player.increase_natural_velocity(Vector2(player.run_speed, 0))
	else: # inertia_direction == -1
		player.decrease_natural_velocity(Vector2(player.run_speed, 0))
		
	if sign(player.get_natural_velocity().x) != sign(inertia_direction):
		set_state("StandingState")
		return
	
	player.run_speed = 0
	player.set_controller_velocity(Vector2(0, 0))
	

func state_process(delta):
	if is_in_air():
		set_state("JumpingState")
		return
		
	elif player.input_direction != 0:
		set_state("RunningState")
		return
		
	elif player.get_natural_velocity().x == 0:
		set_state("StandingState")
		return
	
	if player.get_natural_velocity().x * inertia_direction > 0:
		if inertia_direction == 1:
			player.decrease_natural_velocity(Vector2(player.GROUND_DRAG * delta, 0))
		else: # inertia_direction == -1
			player.increase_natural_velocity(Vector2(player.GROUND_DRAG * delta, 0))
	else: # natural_velocity < 0
		player.reset_natural_velocity()
	
	# Set velocity caused by player input for handling by character.gd
	player.set_controller_velocity(Vector2(0, 0))
	

func set_state(new_state):
	if exiting == true:
		return
	exiting = true
	
	player.run_speed = 0
	player.reset_natural_velocity()
	
	player.idle_sprite.visible = false
	player.set_state(new_state)
	

func jump():
	player.default_jump()
	set_state("JumpingState")
	

func is_in_air():
	return not player.test_move(player.get_transform(), Vector2(0, 1))
	