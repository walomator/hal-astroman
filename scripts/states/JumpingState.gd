extends "res://scripts/platformer-framework/State.gd"

# This state describes any in which the player is in the air, including falling
# after running off a ledge. It is not limited to voluntarily jumping.
# DEV - This should be two states, one for moving and one for staying still
# BUG - Hitting a wall doesn't reset the built-up velocity
# BUG - Player can double jump to stop locked horizontal acceleration

var fall_locked
var locked_direction
var landing_anim

const AnimationExtended = preload("res://scripts/platformer-framework/animation_extended.gd")
var effect

func _init(controlled_player):
	player = controlled_player
	

func _ready():
	landing_anim = player.get_node("LandingAnim")
	

func start():
	set_state_name("JumpingState")
	player.fall_anim.play()
	player.fall_anim.visible = true
	fall_locked = player.is_fall_locked()
	
	if player.jump_count == 0:
		player.jump_count = 1
	
	locked_direction = player.facing_direction
	

func state_process(delta):
	if fall_locked == false:
		if player.input_direction != 0:
			player.run_speed += player.ACCELERATION * delta
		else:
			player.run_speed = 0
	
	if player.run_speed > player.MAX_RUN_SPEED:
		player.run_speed = player.MAX_RUN_SPEED
	
	# Set velocity caused by player input for handling by character.gd
	if fall_locked:
		player.set_controller_velocity(Vector2(player.run_speed * locked_direction, 0))
	else:
		player.set_controller_velocity(Vector2(player.run_speed * player.input_direction, 0))
	
	# Stop plasma particles from emitting if player is falling
	if player.get_natural_velocity().y > 0:
		_set_boot_rockets(false)
	
	if is_on_ground():
		set_state("SkiddingState")
		return
	
	

func set_state(new_state):
	if exiting == true:
		return
	exiting = true
	
#	if fall_locked == true:
#		player.facing_direction = locked_direction
	
	if new_state == "SkiddingState":
		player.jump_count = 0
		_set_boot_rockets(false)
		_land()
	
	player.fall_anim.stop()
	player.fall_anim.visible = false
	
	player.set_state(new_state)
	

func jump():
#	effect = Effect.new()
#	self.add_child(effect)
#	effect.turn_on_particles(StarEmitter) # DEV - This function doesn't exist yet
	
	_set_boot_rockets(true)
	player.default_jump()
	

func is_on_ground():
	var test = false
	if player.natural_velocity.y >= 0:
		test = player.test_move(player.get_transform(), Vector2(0, 1))
	return test
	

func _land():
	player.landing_anim.play_once()
	player.sounds.play("JumpLanding")
	

func _set_boot_rockets(boolean):
	player.boot_particles.emitting = boolean
	player.boot_particles_2.emitting = boolean
	