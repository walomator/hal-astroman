extends "res://scripts/platformer-framework/State.gd"

func _init(controlled_player):
	player = controlled_player
	

func start():
	set_state_name("StandingState")
	player.idle_sprite.visible = true
	
	player.set_controller_velocity(Vector2(0, 0))
	

func state_process(delta):
	if player.input_direction != 0:
		set_state("RunningState")
		return
	
	elif is_in_air():
		set_state("JumpingState")
		return
	

func set_state(new_state):
	if exiting == true:
		return
	exiting = true
	
	player.idle_sprite.visible = false
	player.set_state(new_state)
	

func jump():
	player.default_jump()
	set_state("JumpingState")
	

func is_in_air():
	return not player.test_move(player.get_transform(), Vector2(0, 1))
	