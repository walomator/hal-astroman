extends "res://scripts/platformer-framework/character.gd"

# BUG - Several buggy things start happening when something like a wall gets
#       in the way of the sliding platform. May be the fault of the 
#       character.gd follower system

const PATH_SPEED = 80
const PATH_LENGTH = 130
var starting_position
var path_distance_travelled = 0
var path_direction = 1
var trajectory = Vector2((PATH_SPEED * path_direction), 0)

func _ready():
	configure_for(PRESET_SLIDING_PLATFORM)
	starting_position = position
	set_controller_velocity(Vector2(trajectory))
	

func _physics_process(delta):
	path_distance_travelled = (position.x - starting_position.x) * path_direction
	
	if path_distance_travelled >= PATH_LENGTH:
		starting_position = position
		turn_around()
	
	if position.y != starting_position.y:
		position.y = starting_position.y


func turn_around():
	path_direction = -path_direction
	set_controller_velocity(Vector2((PATH_SPEED * path_direction), 0))
	

func handle_player_hit_enemy_top(player, enemy): # DEV - Fix collision_handler so an empty function is not needed on unreactive entities
	pass


func die():
	self.queue_free()
