# Hal Astroman

## About

A demo game for my [Godot Platformer Engine](https://gitlab.com/walomator/platformer-framework).
May it someday be awesome nonetheless.

## Legal Notice
This program is free software (GPLv3). See the accompanying LICENSE if you'd
like to know your rights.

All non-code content is released under the Creative Commons CC0 license,
effectively making it public domain, unless otherwise specified.

Exceptions include:
* BitPotion font by Joel Rogers is CC-BY (https://joebrogers.itch.io/bitpotion).
Consider donating!

See the accompanying LICENSE-CC-BY and LICENSE-CC0 for the licenses' legal
code and/or read an easy to understand summary on the Creative Commons website.
* [CC-BY](https://creativecommons.org/licenses/by/4.0)
* [CC0](https://creativecommons.org/publicdomain/zero/1.0/)
